" Moving your Vim cursor around using the arrow keys is a bad habit, 
" and like many bad habits it’s a difficult one to break!
" Please help me:
"noremap <Up> <NOP>
"noremap <Down> <NOP>
"noremap <Left> <NOP>
"noremap <Right> <NOP>

" =================== General vim settings ===================

" Check thatNord Theme is installed before setting it as colorscheme
if !empty(glob('~/.vim/pack/themes/start/nord.vim'))
    set background=dark
    colorscheme nord
endif

" Disable Background Color Erase (BCE) so that color schemes
" work properly when Vim is used inside tmux and GNU screen.
" https://sunaku.github.io/vim-256color-bce.html
if &term =~ '256color'
    set t_ut=
endif

" Use indent of current line for new line
set autoindent

" Make backspace work as expected
set backspace=indent,eol,start

map <F1> gg=G<C-o><C-o>

" Make white-space more visible
"set list listchars=tab:\»\ ,nbsp:⎵,trail:·

" Auto complete can be super slow because it looks through a lot of files
" This option disables looking through all included files
" https://stackoverflow.com/questions/2169645/vims-autocomplete-is-excruciatingly-slow
" set complete-=i

" When a line starts with a tab, a press on backspace will delete the tab,
" even if that tab was expanded into spaces using expandtab
set smarttab

" show existing tab with 4 spaces width
set tabstop=4

" when indenting with '>', use 4 spaces width
set shiftwidth=4

" On pressing tab, insert 4 spaces
" Not set because tabs vs spaces
" set expandtab

" Make vim wait for next key to determine if this key is part of longer command
set ttimeout
" ... but only for 100ms
set ttimeoutlen=100

" Highlight words when searching
set incsearch
set hlsearch

" Allow dirty buffers
set hidden

" Better line wrapping
set textwidth=120

" Always show at least three lines above and below cursor
set scrolloff=3

" Always show at least three columns to left and right of cursor
set sidescrolloff=3

" Show as much as last line as possible, using @@@ to indicate longer line
set display+=lastline

" Remember 1000 commands
set history=1000

" Do not let vim wrap lines
set nowrap

" Wildmenu
set wildmenu

" Navigate splits
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Always use utf-8
set encoding=utf-8

" Sometimes a mouse is a cool thing to have
set mouse=a

" Do not create swap files
set nobackup
set nowritebackup
set noswapfile
set autoread
au FocusGained,BufEnter * :checktime

" Set vim title to default (filename)
set title

" Show absolute line number on current line and relative everywhere else
set number
set nu rnu
au InsertEnter * :set nu nornu
au InsertLeave * :set nu rnu

" searches are case insensitive...
set ignorecase
" unless they contain at least one capital letter
set smartcase

" Improve syntax highlighting performance
" http://vim.wikia.com/wiki/Fix_syntax_highlighting
"syntax on
" syntax sync minlines=200

" Improve scrolling behaviour for long lines
set synmaxcol=300

" Line marking at 80 characters
set colorcolumn=80

" Highlight of numbers and active line
set cursorline

" Kept for reference - how to clear formatting on CursorLine
"hi clear CursorLine
"hi clear CursorLineNr
" Remove styling set by colorscheme
"hi CursorLine cterm=none
"hi CursorLineNr cterm=bold
hi visual term=reverse cterm=reverse

" Use control space for omni content assist
if has("gui_running")
    " C-Space seems to work under gVim on both Linux and win32
    inoremap <C-Space> <C-x><C-o>
else " no gui
  if has("unix")
    inoremap <Nul> <C-x><C-o>
  else
  " I have no idea of the name of Ctrl-Space elsewhere
  endif
endif

let mapleader=","

" ==================== Buffergator  ===================

map <Leader>, :BuffergatorToggle<CR>

" ==================== Limelight ===================

" Default: 0.5
"let g:limelight_default_coefficient = 0.5

" Number of preceding/following paragraphs to include (default: 0)
" let g:limelight_paragraph_span = 1

" ==================== NerdTree ====================

map <leader>n :NERDTreeToggle<CR>
" Open nerd tree automatically and move focus to last buffer
"autocmd vimenter * NERDTree | wincmd p
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() < 1 && !exists("s:std_in") | NERDTree | wincmd p | endif
nnoremap <silent> <Leader>m :NERDTreeFind<CR>

" Automatically close nerd tree if last remaining window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Auto close nerd tree when opening a file
let NERDTreeQuitOnOpen = 0

let NERDTreeMinimalUI = 1

" Mirror tree accross tabs
let NERDTreeMirror = 1

" =================== TagBar ===================

nmap <F9> :TagbarOpenAutoClose<CR>
nmap <F8> :TagbarToggle<CR>

" ============ Misc ============

filetype plugin indent on
syntax on
hi visual term=reverse cterm=reverse

" Javascript stuff
augroup js_config " {
	autocmd!
	autocmd BufNewFile,BufRead *.vue set ft=javascript
	autocmd BufNewFile,BufRead *.js  set ft=javascript
augroup END " }
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=0 expandtab

" ============ Go related stuff ============

"au filetype go inoremap <buffer> . .<C-x><C-o>
au FileType go nmap <leader>r <Plug>(go-run)
au FileType go nmap <leader>b <Plug>(go-build)
au FileType go nmap <leader>t <Plug>(go-test)
au FileType go nmap <leader>c <Plug>(go-coverage)
au FileType go nmap <Leader>gd <Plug>(go-doc)
au FileType go nmap <Leader>s <Plug>(go-implements)
au FileType go nmap <Leader>i <Plug>(go-info)
au FileType go nmap <Leader>e <Plug>(go-rename)
au FileType go nmap <Leader>l :GoMetaLinter<CR>
au FileType go nmap <Leader>a :GoAddTags<CR>
"let g:go_fmt_command = "goimports"
let g:go_highlight_types = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_auto_type_info = 1
let g:go_doc_balloon = 1
let g:go_doc_popup_window = 1
"let g:go_auto_sameids = 1
set updatetime=100
nnoremap <silent> <Leader>j :GoDecls<CR>
nnoremap <silent> <Leader>J :GoDeclsDir<CR>

" ========== LIGHTLINE ==========

" Always show status line
set laststatus=2

" Do not show mode message
set noshowmode

" Basically copied from lightline readme with removed special characters
" https://github.com/itchyny/lightline.vim
let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch', 'readonly', 'filename', 'modified' ] ]
      \ },
      \ 'component': {
      \   'readonly': '%{&readonly?" RO ":""}',
      \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
      \   'gitbranch': '%{FugitiveHead()}'
      \ },
      \ 'component_visible_condition': {
      \   'readonly': '(&filetype!="help"&& &readonly)',
      \   'modified': '(&filetype!="help"&&(&modified||!&modifiable))',
      \   'gitbranch': '(exists("*fugitive#head") && ""!=fugitive#head())'
      \ }}

source $HOME/.vim/snippets/go.vim

" FZF configuration
if !empty(glob('/opt/local/share/fzf/vim'))
	set rtp+=/opt/local/share/fzf/vim
endif

nnoremap <silent>         ; :Buffers<CR>
nnoremap <silent> <Leader>f :Files<CR>
nnoremap <silent> <Leader>R :Tags<CR>
nnoremap <silent> <Leader>g :Commits<CR>

" ========== Explore / Netwr ==========

let g:netrw_banner = 0
let g:netrw_liststyle = 3 " Tree-style
let g:netrw_browse_split = 4 " Open in previous window
let g:netrw_altv = 1
let g:netrw_winsize = 25 " Width in percent
"augroup ProjectDrawer
"  autocmd!
"  autocmd VimEnter * :Vexplore
"augroup END
